package com.hospital.weixin.pojo;

/** 
 * 菜单 
 *  
 * @author 
 * 
 */  
public class Menu {  
    private Button[] button;  
  
    public Button[] getButton() {  
        return button;  
    }  
  
    public void setButton(Button[] button) {  
        this.button = button;  
    }  
}  
