package com.hospital.weixin.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hospital.weixin.pojo.AccessToken;
import com.hospital.weixin.pojo.Button;
import com.hospital.weixin.pojo.CommonButton;
import com.hospital.weixin.pojo.ComplexButton;
import com.hospital.weixin.pojo.Menu;
import com.hospital.weixin.pojo.ViewButton;
import com.hospital.weixin.util.WeixinUtil;

/** 
 * 菜单管理器类 
 *  
 * @author 
 * @date 
 */  
public class MenuManager {  
    private static Logger log = LoggerFactory.getLogger(MenuManager.class);  
  
    public static void main(String[] args) {  
        // 第三方用户唯一凭证  
        String appId = "wxd3c267883821efd6";  
        // 第三方用户唯一凭证密钥  
        String appSecret = "6d479bd118c425b1de90aa51493462e5";  
  
        // 调用接口获取access_token  
        AccessToken at = WeixinUtil.getAccessToken(appId, appSecret);  
  
        if (null != at) {  
            // 调用接口创建菜单  
            int result = WeixinUtil.createMenu(getMenu(), at.getToken());  
  
            // 判断菜单创建结果  
            if (0 == result)  
                log.info("菜单创建成功！");  
            else  
                log.info("菜单创建失败，错误码：" + result);  
        }  
    }  
  
    /** 
     * 组装菜单数据 
     *  
     * @return 
     */  
    private static Menu getMenu() {  
        ViewButton btn11 = new ViewButton();  
        btn11.setName("服务事项");  
        btn11.setType("view");  
        btn11.setUrl("http://www.xieyuna.com/jfinal_weixin/service");  
  
        CommonButton btn12 = new CommonButton();  
        btn12.setName("优质产品");  
        btn12.setType("click");  
        btn12.setKey("12");  
  
        CommonButton btn13 = new CommonButton();  
        btn13.setName("先进仪器");  
        btn13.setType("click");  
        btn13.setKey("13");  
  
        CommonButton btn14 = new CommonButton();  
        btn14.setName("公司历程");  
        btn14.setType("click");  
        btn14.setKey("14");
        
        CommonButton btn15 = new CommonButton();  
        btn15.setName("微官网");  
        btn15.setType("click");  
        btn15.setKey("15"); 
  
        CommonButton btn21 = new CommonButton();  
        btn21.setName("星级代言人");  
        btn21.setType("click");  
        btn21.setKey("21");  
  
        CommonButton btn22 = new CommonButton();  
        btn22.setName("找到我们");  
        btn22.setType("click");  
        btn22.setKey("22");  
  
        CommonButton btn23 = new CommonButton();  
        btn23.setName("美丽热线");  
        btn23.setType("click");  
        btn23.setKey("23");  
  
        CommonButton btn24 = new CommonButton();  
        btn24.setName("最新优惠");  
        btn24.setType("click");  
        btn24.setKey("24");  
  
        CommonButton btn25 = new CommonButton();  
        btn25.setName("投诉建议");  
        btn25.setType("click");  
        btn25.setKey("25");  
  
        CommonButton btn33 = new CommonButton();  
        btn33.setName("绽放献礼");  
        btn33.setType("click");  
        btn33.setKey("33");  
  
        
  
        ComplexButton mainBtn1 = new ComplexButton();  
        mainBtn1.setName("科学专业");  
        mainBtn1.setSub_button(new Button[] { btn11, btn12, btn13, btn14 ,btn15});  
  
        ComplexButton mainBtn2 = new ComplexButton();  
        mainBtn2.setName("高端优质");  
        mainBtn2.setSub_button(new Button[] { btn21, btn22, btn23, btn24, btn25 });  
  
        ComplexButton mainBtn3 = new ComplexButton();  
        mainBtn3.setName("绽放献礼");  
        
  
        /** 
         * 这是公众号胖纸和帅哥的故事目前的菜单结构，每个一级菜单都有二级菜单项<br> 
         *  
         * 在某个一级菜单下没有二级菜单的情况，menu该如何定义呢？<br> 
         * 比如，第三个一级菜单项不是“更多体验”，而直接是“幽默笑话”，那么menu应该这样定义：<br> 
         * menu.setButton(new Button[] { mainBtn1, mainBtn2, btn33 }); 
         */  
        Menu menu = new Menu();  
        menu.setButton(new Button[] { mainBtn1, mainBtn2, btn33 });  
  
        return menu;  
    }  
}  
