<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

	<head>
		<meta charset="utf-8">
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta name="apple-mobile-web-app-status-bar-style" content="black" />
		<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-2.0.3.min.js"></script>
		<title>欧亚美创美容</title>
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/style.css">
		<script>
			var imgUrl = 'http://lg-ieic.com/productdemo/ouya/img/wxtb.jpg';
			var lineLink = 'http://lg-ieic.com/productdemo/ouya/index.html';
			var descContent = "30年纤体美容经验  香港百分百信心之选 Powered by 智慧园区";
			var shareTitle = '欧亚美创美容中心';
			var appid = '';

			function shareFriend() {
				WeixinJSBridge.invoke('sendAppMessage', {
					"appid": appid,
					"img_url": imgUrl,
					"img_width": "120",
					"img_height": "120",
					"link": lineLink,
					"desc": descContent,
					"title": shareTitle
				}, function(res) {
					_report('send_msg', res.err_msg);
				})
			}

			function shareTimeline() {
				WeixinJSBridge.invoke('shareTimeline', {
					"img_url": imgUrl,
					"img_width": "120",
					"img_height": "120",
					"link": lineLink,
					"desc": descContent,
					"title": shareTitle
				}, function(res) {
					_report('timeline', res.err_msg);
				});
			}

			function shareWeibo() {
				WeixinJSBridge.invoke('shareWeibo', {
					"content": descContent,
					"url": lineLink,
				}, function(res) {
					_report('weibo', res.err_msg);
				});
			}
			document.addEventListener('WeixinJSBridgeReady', function onBridgeReady() {

				WeixinJSBridge.on('menu:share:appmessage', function(argv) {
					shareFriend();
				});

				WeixinJSBridge.on('menu:share:timeline', function(argv) {
					shareTimeline();
				});

				WeixinJSBridge.on('menu:share:weibo', function(argv) {
					shareWeibo();
				});
			}, false);
		</script>
	</head>

	<body>
		<div class="scroll" id="scroll" style="display:none;"></div>
		<div class="wrap-contain" id="all-wrap">
			<div class="pulling-top pulling-fixed">
				<a href="index.html" class="sss-home"></a>
			</div>
			<div class="pulling-top">
				<a href="index.html" class="sss-home"></a>
			</div>
			<div class="pulling-down">
				<div class="pulling-title">
					<div class="pullingmenu01"> <span>美胸护理</span> <em class="right"></em> </div>
				</div>
				<div class="pulling-content  my-padding-div" style="display:none;">
					<p class="p-img">
						<img src="img/meixong.jpg">
					</p>
					<div class="p-samllimg" style="width:100%;">
						<p>
							<img src="img/09.png">
						</p>
						<p class="spe-p">&nbsp;</p>
						<p>
							<img src="img/10.png">
						</p>
						<p class="spe-p">&nbsp;</p>
						<p>
							<img src="img/11.png">
						</p>
						<p class="spe-p">&nbsp;</p>
						<p >
							<img class="last" src="img/12.png">
						</p>
					</div>
					<div class="clear"></div>
					<p class="p-font">欧、日、美数以万计女士的成功丰胸方法，由拥有先进设备及丰富经验的专业人士，即时拥有丰满自然、坚挺而富弹性的乳房，安全可靠，持久不变。乳房问题主要分为原发性乳房发育不良及后天性乳房变形。</p>
				</div>
			</div>
			<div class="pulling-down">
				<div class="pulling-title">
					<div class="pullingmenu02"> <span>去皱抗衰老</span> <em class="right"></em> </div>
				</div>
				<div class="pulling-content  my-padding-div" style="display:none;">
					<p class="p-img">
						<img src="img/quzao.jpg">
					</p>
					<div class="p-samllimg" style="width:100%;">
						<p>
							<img src="img/21.png">
						</p>
							<p class="spe-p">&nbsp;</p>
						<p>
							<img src="img/22.png">
						</p>
							<p class="spe-p">&nbsp;</p>
						<p>
							<img src="img/23.png">
						</p>
							<p class="spe-p">&nbsp;</p>
						<p>
							<img class="last" src="img/24.png">
						</p>
					</div>
					<div class="clear"></div>
					<p class="p-font">面对胶原流失及基因问题，针对细胞外基质（简称ECM）来补充营养成分，例如蛋白聚糖、维生素、氨基酸、透明质酸等，加速真皮层胶原蛋白的再生及合成，并促进胶原纤维、弹力纤维及网状纤维的新陈代谢，有效去除面部松弛、皱纹及暗哑问题。</p>
				</div>
			</div>
			<div class="pulling-down">
				<div class="pulling-title">
					<div class="pullingmenu03"> <span>暗疮治理</span> <em class="right"></em> </div>
				</div>
				<div class="pulling-content my-padding-div" style="display:none;">
					<p class="p-img">
						<img src="img/ancun.jpg">
					</p>
					<div class="p-samllimg" style="width:100%;">
						<p>
							<img src="img/17.png">
						</p>
						<p class="spe-p">&nbsp;</p>
						<p>
							<img src="img/18.png">
						</p>
						<p class="spe-p">&nbsp;</p>
						<p>
							<img src="img/19.png">
						</p>
						<p class="spe-p">&nbsp;</p>
						<p>
							<img class="last" src="img/20.png">
						</p>
					</div>
					<div class="clear"></div>
					<p class="p-font">独特而有效的暗疮疗法，能在皮肤下快速消除有害物质，有效减少皮肤分泌过多的油脂，并集中补充流失的水分，减低皮肤干涸及敏感，使皮肤明亮通透，暗疮自然难以滋生。</p>
				</div>
			</div>
			<div class="pulling-down">
				<div class="pulling-title">
					<div class="pullingmenu04"> <span>美白祛斑</span> <em class="right"></em> </div>
				</div>
				<div class="pulling-content my-padding-div" style="display:none;">
					<p class="p-img">
						<img src="img/meibai--.jpg">
					</p>
					<div class="p-samllimg" style="width:100%;">
						<p>
							<img src="img/05.png">
						</p>
						<p class="spe-p">&nbsp;</p>
						<p>
							<img src="img/06.png">
						</p>
						<p class="spe-p">&nbsp;</p>
						<p>
							<img src="img/07.png">
						</p>
						<p class="spe-p">&nbsp;</p>
						<p>
							<img class="last" src="img/08.png">
						</p>
					</div>
					<div class="clear"></div>
					<p class="p-font">独特而有效的美白祛斑疗法，能在皮肤下快速消除有害物质，避免皮肤出现过敏现象，绝无副作用。消除色斑，快速美白，效果完美。</p>
				</div>
			</div>
			<div class="pulling-down">
				<div class="pulling-title">
					<div class="pullingmenu05"> <span>眼部修护</span> <em class="right"></em> </div>
				</div>
				<div class="pulling-content my-padding-div" style="display:none;">
					<p class="p-img">
						<img src="img/ybu.jpg">
					</p>
					<p class="p-font">专注眼部护理30年，无论是眼袋、眼纹、眼肚、黑眼圈、干纹及双眼皮等都得心应手，是信心必然之选。全程由专业顾问跟进，信心十足。令您眼部恢复魅力，电力十足。</p>
				</div>
			</div>
			<div class="pulling-down">
				<div class="pulling-title">
					<div class="pullingmenu06"> <span>纤体瘦身</span> <em class="right"></em> </div>
				</div>
				<div class="pulling-content my-padding-div" style="display:none;">
					<p class="p-img">
						<img src="img/qti--.jpg">
					</p>
					<div class="p-samllimg" style="width:100%;">
						<p>
							<img src="img/01.png">
						</p>
						<p class="spe-p">&nbsp;</p>
						<p>
							<img src="img/02.png">
						</p>
						<p class="spe-p">&nbsp;</p>
						<p>
							<img src="img/03.png">
						</p>
						<p class="spe-p">&nbsp;</p>
						<p>
							<img class="last" src="img/04.png">
						</p>
					</div>
					<div class="clear"></div>
					<p class="p-font">拥有二十多种欧美纤体美容仪器，已帮助数万位客户瘦身成功；透过使用不同的欧美纤体美容仪器，可选择全身或局部瘦身，包括：瘦全身，瘦面、修腹、减手臂、减臀、减大腿、减小腿。</p>
				</div>
			</div>
			<div class="pulling-down">
				<div class="pulling-title">
					<div class="pullingmenu07"> <span>光学脱毛</span> <em class="right"></em> </div>
				</div>
				<div class="pulling-content my-padding-div" style="display:none;">
					<p class="p-img">
						<img src="img/guanxue.jpg">
					</p>
					<div class="p-samllimg" style="width:100%;">
						<p>
							<img src="img/13.png">
						</p>
						<p class="spe-p">&nbsp;</p>
						<p>
							<img src="img/14.png">
						</p>
						<p class="spe-p">&nbsp;</p>
						<p>
							<img src="img/15.png">
						</p>
						<p class="spe-p">&nbsp;</p>
						<p>
							<img class="last" src="img/16.png">
						</p>
					</div>
					<div class="clear"></div>
					<p class="p-font">以新除毛技术，由专业团队依据个人毛质、毛量、透过精密专业分析判断。光学脱毛能稳定均匀地打在治疗部位，过程安全可靠，不会有热能量过渡集中的问题，能有效达到最精准的治疗。</p>
				</div>
			</div>
			<div class="pulling-down">
				<div class="pulling-title">
					<div class="pullingmenu08"> <span>疤痕修复</span> <em class="right"></em> </div>
				</div>
				<div class="pulling-content my-padding-div" style="display:none;">
					<p class="p-img">
						<img src="img/bahen.jpg">
					</p>
					<p class="p-font">治疗疤痕主要是以磨皮或镭射换肤，藉由磨皮破坏后，重新造建肌肤，康复期却是比较漫长；而本中心用心的机械动力之高科技，能刺激纤维母细胞，使胶蛋白增生，真皮层得以迅速再生，令肌肤回复以往的弹力，产生极佳的淡化及抚平疤痕的效果。</p>
				</div>
			</div>
			<div class="pulling-down">
				<div class="pulling-title"  id="lkts">
					<div class="pullingmenu09"> <span>轮廓提升</span> <em class="right"></em> </div>
				</div>
				<div class="pulling-content my-padding-div" style="display:none;">
					<p class="p-img">
						<img src="img/lk.jpg">
					</p>
					<p class="p-font">随着年龄增长，面上的骨胶原和弹力蛋白会逐渐流失，随之而来的就是皮下脂肪萎缩及失去弹性，令皮肤日渐松弛。本项目能有效提升面额、面珠、眼袋凹陷等，可抚平本身疤痕。提升面部肌肉、提升下巴轮廓，无需开刀，有效回复昔日光彩。</p>
				</div>
			</div>
			<div class="pulling-down">
				<div class="pulling-title">
					<div class="pullingmenu10"> <span>产后护理</span> <em class="right"></em> </div>
				</div>
				<div class="pulling-content my-padding-div" style="display:none;">
					<p class="p-img">
						<img src="img/cp.jpg">
					</p>
					<p class="p-font">专业产后修复中心，针对生育后体型变化，如：胸部松弛下垂，腰部赘肉横生，妊娠纹等，无需开刀、服药或节食。采用欧美最新技术及仪器，坚持为产后女士提供最优质及护理效果，并不断为员工作出专业培训，紧贴市场最新资讯，为客户带来更大信心。</p>
				</div>
			</div>
            <a href="tel:4000822988"><img src="img/lx.png" width="100%"></a>
			<p class="copyright"><span>版权所有：欧亚美创</span>&nbsp;&nbsp; <span>整合推广：<a href="http://www.wiseyq.com/">智慧园区</a></span>
			</p>
		</div>
		<script type="text/javascript">
            function getParam(name){
                var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
                var r = window.location.search.substr(1).match(reg);  //匹配目标参数
                if (r != null) return unescape(r[2]);
                return null; //返回参数值
            }
			$(function() {

				$(".pulling-title").bind("click", function() {
					var sibDiv = $(this).siblings();
					if (sibDiv.css("display") == "none") {
						$(this).parent().siblings().find(".pulling-content").hide();
						$(this).parent().siblings().find(".down").removeClass("down").addClass("right");
						$(this).find("em").removeClass("right").addClass("down");
						sibDiv.show();
						$("html,body").animate({
							scrollTop: $(this).offset().top - 50
						}, 500);
					} else {
						$(this).find("em").removeClass("down").addClass("right");
						sibDiv.hide();
					}
				});

                var slideNum=getParam("num");
				if(slideNum==null){
				
				}else{
					var list=$("#all-wrap").find(".pulling-down");
                    list.eq(slideNum).find(".pulling-title").click();
                    console.log(list);
				}

			});
			$(function() {
				showScroll();

				function showScroll() {
					$(window).scroll(function() {
						var scrollValue = $(window).scrollTop();
						scrollValue > 20 ? $('#scroll').fadeIn() : $('#scroll').fadeOut();
					});
					$('#scroll').click(function() {
						$("html,body").animate({
							scrollTop: 0
						}, 200);
					});
				}
			})
		</script>
	</body>

</html>