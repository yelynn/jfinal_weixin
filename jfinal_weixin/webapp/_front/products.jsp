<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta charset="utf-8">
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-2.0.3.min.js"></script>
<title>欧亚美创美容</title>
<link rel="stylesheet" type="text/css" href="css/apart.css">
<script>
var imgUrl = 'http://lg-ieic.com/productdemo/ouya/img/wxtb.jpg';
var lineLink = 'http://lg-ieic.com/productdemo/ouya/index.html';
var descContent = "30年纤体美容经验  香港百分百信心之选 Powered by 智慧园区";
var shareTitle = '欧亚美创美容中心';
var appid = '';
function shareFriend() {
    WeixinJSBridge.invoke('sendAppMessage',{
                            "appid": appid,
                            "img_url": imgUrl,
                            "img_width": "120",
                            "img_height": "120",
                            "link": lineLink,
                            "desc": descContent,
                            "title": shareTitle
                            }, function(res) {
                            _report('send_msg', res.err_msg);
                            })
}
function shareTimeline() {
    WeixinJSBridge.invoke('shareTimeline',{
                            "img_url": imgUrl,
                            "img_width": "120",
                            "img_height": "120",
                            "link": lineLink,
                            "desc": descContent,
                            "title": shareTitle
                            }, function(res) {
                            _report('timeline', res.err_msg);
                            });
}
function shareWeibo() {
    WeixinJSBridge.invoke('shareWeibo',{
                            "content": descContent,
                            "url": lineLink,
                            }, function(res) {
                            _report('weibo', res.err_msg);
                            });
}
document.addEventListener('WeixinJSBridgeReady', function onBridgeReady() {

        WeixinJSBridge.on('menu:share:appmessage', function(argv){
            shareFriend();
            });

        WeixinJSBridge.on('menu:share:timeline', function(argv){
            shareTimeline();
            });

        WeixinJSBridge.on('menu:share:weibo', function(argv){
            shareWeibo();
            });
        }, false);
</script>
</head>
<body>
	<div class="scroll" id="scroll" style="display:none;"></div>	
<div class="wrap-contain" id="all-wrap">
  <div class="pulling-top2"><a href="index.html" class="sss-home"></a></div>
  <div class="pulling-top2 pulling-fixed"><a href="index.html" class="sss-home"></a></div>
  <div class="pulling-down">
    <div class="pulling-title">
      <div class="pullingmenu11"> <em class="right"></em> </div>
    </div>
    <div class="pulling-content" style="display:none;">
      <p class="p-img"><img src="img/ise.jpg"></p>
      <div class="p-font"> <span>品牌理念：</span>
        <p>1、表皮</p>
        <p class="xiangx">一般保养品只能对表面皮肤带来修复效果，但是I’SEANA透过独特技术可将营养直接注入真皮肤，使其有效成分能达至最大效果。</p>
        <p>2、真皮</p>
        <p class="xiangx"> 我们深信护肤犹如楼层建筑，只有将基层打造好了，才能建造健康，造就更具光彩的肌肤。同时运用创新的活性技术将使肌肤重塑昔日光彩。让用家更自信，更有魅力。</p>
        <p>3、肌肤再生应由深层开始</p>
        <p class="xiangx"> I’SEANA透过专利技术及成份能将营养注入肌肤深层，为您带来如丝般再生的嫩肌。</p>
        <p>4、瑞士美容原料的宝库</p>
        <p class="xiangx"> 保证原料的安全性与素质是每个美容业者所守护的根本，我们通过严谨的流程与制作为客户带来顶级的美容享受。</p>
        <p>5、I’SEANA的美容革命</p>
        <p class="xiangx"> 我们使用瑞士经典配方，无论任何年龄、肤质，为所有女性带来深层强效抗衰老、修复力量，守护女性幼嫩的肌肤及透亮性，保持水嫩细致无暇。每款I’SEANA产品都能让您期待每天享受护肤的乐趣。</p>
        <div class="orange-font"> <span class="or-top"></span> <span class="or-first">坚持练习嘴唇操 有效减少小皱纹</span>
          <p class="or-font">虽然人的衰老是从内部器官开始，但在脸上的体现却是最明显的。每天坚持练习嘴唇操，就可有效减少面部皱纹，并保持面色红润。</p>
          <span class="or-strong">开闭嘴唇法</span> 　 　
          <p class="or-font">将嘴最大限度地张开，发“啊”声或呵气，然后再闭合，有节奏地一张一合，每次连续100下，或持续2～3分钟。每天早晚一次，每次说30～40遍“欧咿”声。</p>
          <span class="or-strong">擦搓嘴唇法</span> 　
          <p class="or-font">把嘴唇闭好，用一只手的两个手指在嘴唇外轻轻地擦或搓，一直到擦搓的地方变红又感觉发热之后停止动作。这个方法能有效维持嘴部形态，还能使口腔或牙龈的血液循环得到改善。</p>
          <span class="or-strong">闭唇鼓腮法</span> 　　
          <p class="or-font">鼓起左腮，用力吹气，使气流通过左嘴角呼出。再鼓起右腮，用力吹气，使气流通过右嘴角呼出，反复多次；闭紧嘴唇，两腮用力鼓起，用食指按住嘴角，然后收回，反复多次。可防止腮部肌肉萎缩塌陷。</p>
          </p>
        </div>
      </div>
    </div>
  </div>
  <div class="pulling-down">
    <div class="pulling-title">
      <div class="pullingmenu12"> <em class="right"></em> </div>
    </div>
    <div class="pulling-content" style="display:none;">
      <p class="p-img"><img src="img/ce.jpg"></p>
      <div class="p-font"> <span>拥有健康细胞是年轻肌肤的第一步：</span>
      <p class="xiangx">要想阻挡时间在您的俏脸上留下痕迹，解决肌肤各种问题，我们更需要了解肌肤与细胞的微妙关系。瑞士CE细胞原液能成功破解细胞结构与肌肤老化的关系，可持续供给细胞的营养。维持及调动细胞正常代谢功能，使细胞衰老周期延缓，从而促进细胞生长及复原，肌肤呈现紧致细滑，放射持久青春活力。配合CE细胞驻颜+微针疗程，效果更好。让您终极体验逆转时光的美妙感觉。</p>
       <div class="orange-font"> <span class="or-top"></span> <span class="or-first">轻松吃掉眼袋无压力</span>
          <p class="or-font">组织松弛型，即下眼睑皮肤、皮下组织、肌肉及眶膈松弛，下眼睑脂肪堆积，形成袋状突起称眼袋。这是人体老化的具体表现之一，常见于45岁以后的中老年人，30岁+亦可能开始出现。下眼睑浮肿，是由于眼部皮肤很薄，很容易发生水肿现象，遗传是一个重要因素，此外，饮食不当、肾脏不好、睡眠不足或疲劳都会造成眼袋出现。这种情况在20岁以后的年轻人中就已经常见，尤其是25岁+，随着年龄增长会愈加明显。只要有心，预防眼袋、尽量延缓它的到来还是很容易做到的。那么，为了远离眼袋，我们应该怎么吃呢？</p>
          <span class="or-strong">注意膳食平衡</span> 　 　
          <p class="or-font">多摄取胶质和蛋白质，为组织细胞的新生提供必要的营养物质，补充肌肤维持活力所必须的能量。如肉类、鱼类、蛋类。切忌盲目节食减肥，以致营养不良或体重突降，大大影响皮肤弹性。</p>
          <span class="or-strong">多吃有助于保护眼睛、富含维生素A和B的食物</span> 　
          <p class="or-font">如胡萝卜、土豆、豆制品和动物肝脏。还有能够补血养血、促进血液循环的如苹果红枣鲜鱼汤，就是一道对抗黑眼圈和眼袋都备受推崇的经典食疗汤。</p>
          <span class="or-strong">维持身体正常的新陈代谢</span> 　　
          <p class="or-font">别忽略有利于排出体内多余水分、消除浮肿的食物，如红豆、冬瓜、薏仁等。</p>
          </p>
        </div>
      </div>
    </div>
  </div>
    <div class="pulling-down">
    <div class="pulling-title">
      <div class="pullingmenu13"> <em class="right"></em> </div>
    </div>
    <div class="pulling-content" style="display:none;">
      <p class="p-img"><img src="img/jin.jpg"></p>
        <div class="p-font"> <span>韩国崭新全脸技术：</span>
        <p class="xiangx">一般保养品只能对表面皮肤带来修复效果，但是I’SEANA透过独特技术可将营养直接注入真皮肤，使其有效成分能达至最大效果。</p>
         <span>PDO提拉效果立竿见影：</span>
        <p class="xiangx">4D轮廓拉提线技术，主要是透过植入PDO医疗微线，能诱发轻度的急性反应，促使皮肤启动修复程序，启动干细胞及聚集血小板，能释放各种生长因子。长期更对胶原蛋白增新，增加血液循环，产生皮肤新效应，达到真正紧实提及肤质改善的效果。</p>
         <span>4D轮廓拉提线技术优势：</span>
        <p>1、PDO医疗线获国际认证，于外科使用多年，安全性极高。</p> 
        <p>2、无需开刀，植线时间简单快捷</p>
        <p>3、不会流血，施针部分只有极轻的微痛感。</p>
        <p>4、复原时间只需数天，亦无伤口。</p>
        <p>5、植线后效果即时明显而见，最重要是效果自然。</p>
        <p>6、无疤痕，无副作用，可立即上班。</p>
        <p>7、能持续诱发胶原蛋白增生。</p>
       <p>8、同时达到拉皮和紧致效果。</p>
       <div class="orange-font"> <span class="or-top"></span> <span class="or-strong">长痘应先调脾胃</span>
          <p class="or-font">生活水平提高了，“面子”愈发重要，人们在皮肤护理上的花费越来越多。但是，“面子”固然重要，“里子”更为关键。皮肤病虽见于皮毛肌肤，但与体内脏腑气血阴阳失调有关，与脾胃功能失调关系尤为密切。</p>
         <p class="or-font">引起脾胃损伤的原因主要有三种：饮食失节，劳逸过度，精神刺激。中医认为“饮食自倍，肠胃乃伤”，饥饱无度、嗜食辛辣肥甘厚腻就会影响脾胃吸收、消化，从而导致皮肤病的发生。过劳或贪图安逸，也会影响脾胃功能，不利于气血的正常运行，严重者导致气血壅滞，表现为皮肤红肿热痛、干燥脱屑、色斑形成等症状。因此，皮肤病患者一定要注意休息，作息规律，切勿长期熬夜。此外，长期、过度的精神刺激，如愤怒、悲哀、忧愁、思虑、恐惧等，都会影响脾胃的消化功能，诱发黄褐斑、白癜风、雀斑、粉刺等病的产生。</p>
         　  <p class="or-font">既然源于脾胃失和，皮肤病患者就要在辨证论治的基础上调理身体：属于湿热症者，要祛湿清热；属于寒湿症者，应温阳、健脾、利湿。</p>
         <p class="or-font">需强调的是，一些爱长痘痘的患者以为长痘都是“上火”，就买来苦寒的泻药来“去火、排毒”，这是大错特错。苦寒的药物往往伤脾胃，效果可能适得其反。</p>
          </p>
        </div>
      </div>
    </div>
  </div>
    <div class="pulling-down">
    <div class="pulling-title">
      <div class="pullingmenu14"> <em class="right"></em> </div>
    </div>
    <div class="pulling-content" style="display:none;">
      <p class="p-img"><img src="img/s-san.jpg"></p>
      <div class="p-font"> <span>两大独特技术：</span>
    <p>崭新结合螺旋X锥体技术</p>
    <p>螺旋细密式散射能量，发挥精密提拉效果</p>
    <p>每发能量以螺旋细密式散射到肌肤不同层面</p>
    <p>1、令能量快速均匀地到达皮肤</p>
<p>2、避免了热量分散的情况，令热能更集中更快地到达目标位置</p>
<p>3、细密式发射能量令每毫米肌肤都发挥提拉效果，每点微细热能连结起来一同发挥收缩、紧致力量</p>
<p>4、使整个轮廓立即紧实提升，V面顿时呈现。</p>
<p>锥体聚焦瞄准皮层爆发力量，点击松弛肌肤，独特的设计，使能量以锥体形能聚焦：</p>
<p>1、每点细密超声波热能精准的对焦在单一点，把能量聚焦、定位成一个极微细，强热能的能量发射。</p>
<p>2、在不同皮层刺激胶原蛋白的增生与重组，有效达到紧致轮廓及抚平纹路的效果</p>
<p>3、瞬间激生肌底的胶原蛋白及组织而对周边组织不造成影响</p>
<p>4、强大能量单一对点，造出集中、定型、巩固的紧致提升效果</p>
        <div class="orange-font"> <span class="or-top"></span> <span class="or-strong">去除雀斑有效的饮食美容法</span>
          <p class="or-font">雀斑多发生于青少年的面、颈等暴露部位，尤好发于肤色白的女青年，其色泽黧黑，针尖或豆粒大小，形如雀卵，故而得名。其春夏加重，秋冬变淡，病因与遗传、内分泌及日光照射有关。雀斑的形成多为肝郁脾虚，肝肾不足所为，当以补益肝肾，疏肝健脾为治。想要有效的去除雀斑，不妨试试以下饮食美容方法。</p>
          <span class="or-strong">1、胡桃芝麻饮</span> 　 　
          <p class="or-font">胡桃30克，芝麻20克，牛乳、豆浆各200毫升，白糖适量。将胡桃仁、芝麻研为细末，与牛乳、豆浆混匀，煮沸饮服，白糖调味，分作2份，早晚各1份，每日1剂。可补益虚损，生津润肠，润肤消斑。</p>
          <span class="or-strong">2、黑白消斑散</span> 　
          <p class="or-font">黑木耳10克，白木耳5克。将二耳共研细末，每次5克，每日3次，蜂蜜水冲饮，连续1月。可消淤化斑，润肤滋肌。</p>
          <span class="or-strong">3、白鸭消斑汤</span> 　　
          <p class="or-font">白鸭1只，山药200克，生地100克，枸杞子30克，调料适量。将白鸭去毛杂骨，洗净，用食盐、胡椒粉、黄酒涂抹鸭体内外，撒上葱姜腌1小时左右后切为丁;山药切片。生地布包，置碗底，而后纳入山药、枸杞、鸭丁，上笼蒸熟服食，每周2-3剂。可补益肝肾，养阴消斑。</p>
          <span class="or-strong">4、枸杞生地散</span> 　　
          <p class="or-font">枸杞子100克，生地30克。将杞子、生地焙干、研末、混匀，每取10克，每日3次，温开水或用白酒适量冲服，连续1月。可补肝肾，去黑斑。</p>　
         <p>小编提醒：在了解了一些美容的常识，相信我们应该对健康美容有了了解。要在拥有美丽的同时更要注重健康。</p>
          
        </div>
      </div>
    </div>
  </div>
  <a href="tel:4000822988"><img src="img/lx.png" width="100%"></a>
  <p class="copyright"><span>版权所有：欧亚美创</span>&nbsp;&nbsp; <span>整合推广：<a href="http://www.wiseyq.com/">智慧园区</a></span></p>
</div>
<script type="text/javascript">
$(function(){
	$(".pulling-title").bind("click",function(){
		var sibDiv=$(this).siblings();
		if(sibDiv.css("display")=="none"){
			$(this).parent().siblings().find(".pulling-content").hide();
			$(this).parent().siblings().find(".down").removeClass("down").addClass("right");
			$(this).find("em").removeClass("right").addClass("down");
			sibDiv.show();
			$("html,body").animate({scrollTop: $(this).offset().top-50},500); 
		}else{
			$(this).find("em").removeClass("down").addClass("right");
			sibDiv.hide();
		}
	});
	$(".pulling-title div").css("height",227/640*$(".pulling-title div").width()+"px");
	$(".pulling-down em").css("top",(227/640*$(".pulling-title div").width()-24)/2+"px");
});
	$(function(){
		showScroll();
		function showScroll(){
			$(window).scroll( function() { 
				var scrollValue=$(window).scrollTop();
				scrollValue >20 ? $('#scroll').fadeIn():$('#scroll').fadeOut();
			} );	
			$('#scroll').click(function(){
				$("html,body").animate({scrollTop:0},200);	
			});	
		}
	})
    </script>
</body>
</html>